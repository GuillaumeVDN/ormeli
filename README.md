# Ormeli

A full-stack web application allowing doctors, pharmacists and patients to easily transmit medical prescriptions.

See screenshots under /screenshots/ to get a general idea of the features.
Made using React, Firebase, in the context of a course project for EFREI Paris with four other students.
